import { GameOfThronesQuizPage } from './app.po';

describe('game-of-thrones-quiz App', () => {
  let page: GameOfThronesQuizPage;

  beforeEach(() => {
    page = new GameOfThronesQuizPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
